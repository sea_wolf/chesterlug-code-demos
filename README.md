# Chester LUG Code Demos

You're looking at all the code examples written for talks and demos
at meetings of the Chester Linux User Group (LUG).

These files may be working code, non-working code, pseudocode, or
something else entirely. They are structured in directories for each
month, although we may have skipped some months' demos. Some
indication in the filename or content of the subject and speaker would
be great to help people remember them.

Feel free to submit your own files, revisions, notes, whatever via
a Pull Request, and the Organisation admins will review.
